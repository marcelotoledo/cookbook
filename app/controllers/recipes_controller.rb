class RecipesController < ApplicationController
  def create # TO-DO: Improve this method.
    data = params[:recipe]

    recipe = Recipe.new(
      title: data[:title],
      recipe_type: data[:recipe_type],
      cuisine_id: data[:cuisine_id],
      difficulty: data[:difficulty],
      cook_time: data[:cook_time],
      ingredients: data[:ingredients],
      method: data[:method]
    )
    recipe.save!

    redirect_to recipe_path(recipe.id)
  end

  def new
    @recipe = Recipe.new
    @cuisines = Cuisine.all
  end

  def show
    @recipe = Recipe.find params[:id]
  end

  # def update
  #   @article = Article.find(params[:id])
  #
  #   if @article.update(article_params)
  #     redirect_to @article
  #   else
  #     render 'edit'
  #   end
  # end
  #
  # def destroy
  #   @article = Article.find(params[:id])
  #   @article.destroy
  #
  #   redirect_to articles_path
  # end
  #
  # private
  #   def article_params
  #     params.require(:article).permit(:title, :text)
  #   end
end
