class CuisinesController < ApplicationController
  def create
    data = params[:cuisine]

    cuisine = Cuisine.new(name: data[:name])
    cuisine.save!

    redirect_to cuisine_path(cuisine.id)
  end

  def new
    @cuisine = Cuisine.new
  end

  def show
    @cuisine = Cuisine.find params[:id]
  end
end
